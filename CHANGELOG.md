# Change Log

## 0.5.1 - 2024-07-09

- [Changed] update requirements

## 0.5 - 2024-07-09

- [New] Add ao_flag, dp3, concat_vis_cube, run_ml_gpr_injtasks
- [New] obsdata: add a few methods to access ml_gpr results
- [Changed] In image task: parametrize padding and taper-edge of wsclean
- [Changed] Add timestamp to log filenames
- [Changed] Update LOFAR-HBA eor_bins
- [Changed] DPPP -> DP3

## 0.4 - 2023-04-28

- [New] Add vis_flagger task. This requires a nenucal-cd >= 0.5

## 0.3 - 2023-03-29

- [New] Add ALO (Astrophysical Lunar Observatory) config template

## 0.2 - 2023-01-27

- [Fixed] Make sure two MSs in a MS list added via psdb add_obs/add_all_obs does not have the same name, that avoid issues with wsclean temp-file management

## 0.1.15 - 2022-11-28

- [Fixed] Image named are based on the MS list index if no SB is given (Close #7)
- [Changed] Update dependencies

## 0.1.14 - 2022-09-29

- [New] Add ml_gpr.use_v_dt_as_noise parameter

## 0.1.13 - 2022-09-25

- [New] Add gpr.use_v_dt_as_noise parameter
- [Doc] Add unit to a few psdb options

## 0.1.12 - 2022-07-25

- [Changed] Use power-spectra config for A12 provided by Bharat.

## 0.1.11 - 2022-07-25

- [Fixed] Add possibility to image XX, YY, XY and YX.

## 0.1.10 - 2022-02-07

- [Fixed] imaging: change nwlayers to 1000 to reduce gridding errors

## 0.1.9 - 2022-01-20

- [Fixed] relax some python package requirement
- [Fixed] psdb: check that current directory is empty when calling init
- [Fixed] psdb add: handle empty lines

## 0.1.8  - 2020-04-30

- [Changed] nenucal.msutils has been moved from libpipe.msutils.
